(function (angular) {
  'use strict';
  
	  angular.module('formFlickr', ['ngMaterial'])
		.controller('FlickrController', ['$scope', '$http', function ($scope, $http) {

		$scope.availableOptions = [
			{ name:'Animals', namegroup: 'Pets', id:'Animals',valueName: 'Animals' },
			{ type: 'Pets', name: 'Guppy', id: 'Guppy' }, 
			{ type: 'Pets', name: 'Parrot', id: 'Parrot' }, 
			{ type: 'Pets', name: 'Goldfish', id: 'Goldfish' },
			{ type: 'Pets', name: 'Dog', id: 'Dog' },
			{ type: 'Pets', name: 'Cat', id: 'Cat' },   
			{ type: 'Wild animals', name: 'Tiger', id: 'Tiger' },
			{ type: 'Wild animals', name: 'Ant', id: 'Ant' },
			{ type: 'Wild animals', name: 'Tetra', id: 'Tetra' },
			{ type: 'Wild animals', name: 'Mongoose', id: 'Mongoose' },
			{ type: 'Domestic animals', name: 'Cow', id: 'Cow' },
			{ type: 'Domestic animals', name: 'Pig', id: 'Pig' },
			{ type: 'Domestic animals', name: 'Goat', id: 'Goat' },
			{ type: 'Domestic animals', name: 'Horse', id: 'Horse' },
			{ name:'Food', namegroup: 'Fast food', id:'Food',valueName: 'Food' },
			{ type: 'Fast food', name: 'Cheeseburger', id: 'Cheeseburger' },
			{ type: 'Fast food', name: 'Hamburger', id: 'Hamburger' },
			{ type: 'Dessert', name: 'Chocolate', id: 'Chocolate' },
			{ type: 'Dessert', name: 'Cookie', id: 'Cookie' },
			{ type: 'Dessert', name: 'Cake', id: 'Cake' },
			{ type: 'Dessert', name: 'Pie', id: 'Pie' },
			{ name:'Vechicle', namegroup: 'Motorcycle', id:'Vechicle',valueName: 'Vechicle' },
			{ type: 'Motorcycle', name: 'Harley Davidson', id: 'Harley Davidson' },
			{ type: 'Car', name: 'Lamborghini', id: 'Lamborghini' },
			{ type: 'Car', name: 'Ferrari', id: 'Ferrari' },
			{ type: 'Car', name: 'Bugatti', id: 'Bugatti' },
			{ type: 'Car', name: 'BMW', id: 'BMW' },
			{ type: 'Car', name: 'Mercedes', id: 'Mercedes' },
			{ name:'Movie', namegroup: 'Science fiction', id:'Movie',valueName: 'Movie' },
			{ type: 'Science fiction', name: 'Sunshine', id: 'Sunshine' },
			{ type: 'Science fiction', name: 'Interstellar', id: 'Interstellar' },
			{ type: 'Science fiction', name: 'The Moon', id: 'The Moon' },
			{ type: 'Science fiction', name: 'Oblivion', id: 'Oblivion' },
			{ type: 'Science fiction', name: 'Star Trek', id: 'Star Trek' },
			{ type: 'Science fiction', name: 'Star Wars', id: 'Star Wars' }
		];

		$scope.data = {
		model: "" 
		};
				
		$scope.master = {};
		$scope.images = {};

		$scope.concat = function () {
		if (!$scope.searchCriteria){
			$scope.searchCriteria = {};
		}

		if (!$scope.searchCriteria.tags){
			$scope.searchCriteria.tags = "";
		}
			console.log($scope.data.model, $scope.searchCriteria.tags);
			$scope.searchCriteria.tags += ' ' + $scope.data.model;
			if ($scope.searchCriteria.tags == $scope.searchCriteria.tags){
				$scope.searchCriteria.tags = $scope.data.model;
			}

		}

		$scope.search = function () {

		console.log($scope.searchCriteria.tags);

		if (!$scope.searchCriteria.tags) {
			$scope.searchCriteria.tags == "";
		}

		if (!$scope.searchCriteria.mode) {
			$scope.searchCriteria.mode == "all";
		}

		if ($scope.searchCriteria.tags.trim() == "") {
			$scope.searchCriteria.tags = "";
			$scope.master = angular.copy($scope.searchCriteria);
			$scope.form.$submitted = true;
		return false;
		}
		$scope.form.tags.$setUntouched();
		$scope.form.tags.$setValidity();

        // build URL for Flickr API
        var flickrAPI = "http://api.flickr.com/services/feeds/photos_public.gne";
		var key = "57f694132e4714c29a64c9af890b124e";
		
        flickrAPI = flickrAPI + "?jsoncallback=JSON_CALLBACK"
          + "&tags=" + encodeURIComponent($scope.searchCriteria.tags)
          + "&tagmode=" + $scope.searchCriteria.mode
		  + "&api_key=" + key
          + "&format=json";
			console.log(flickrAPI);
        // send AJAX query to Flickr API
        $http.jsonp(flickrAPI)
          .success(function (data, status, headers, config) {
          $scope.images = data;
          $scope.imagesStatus = status;

        })
        
        // trap error if any
          .error(function (data, status, headers, config) {
          $scope.images = data;
          $scope.imagesStatus = status;
        });
        
        // reset form validation
        $scope.form.tags.$setValidity();
      };
      
      // reset form to initial state
      $scope.resetForm = function (form) {
        $scope.form.tags.$setValidity();
        $scope.images = {};
        $scope.searchCriteria = {};
      };

    }]);
})(window.angular);